import 'package:flutter/material.dart';
import 'package:premiumtravel/extreme_widgets/helper/input.dart';

class StandartInput extends StatefulWidget {
  final String id;
  final String labelText;
  final IconData iconName;
  final String hintText;
  final String value;
  final TextInputType keyboardType;

  const StandartInput({
    Key key,
    this.id,
    this.hintText: "",
    this.iconName,
    this.labelText,
    this.value: "",
    this.keyboardType: TextInputType.text,
  }) : super(key: key);

  @override
  _StandartInputWidget createState() => _StandartInputWidget();
}

class _StandartInputWidget extends State<StandartInput> {
  bool enableSuffixIcon = false;
  String value = "";

  final TextEditingController controller = TextEditingController();
  final FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    Widget suffixIconWidget = IconButton(
      icon: Icon(Icons.close),
      onPressed: () {
        controller.text = "";
        setState(() {
          value = "";
        });
      },
    );

    if (enableSuffixIcon == false) {
      suffixIconWidget = null;
    }

    TextField instance = TextField(
      controller: controller,
      focusNode: focusNode,
      autofocus: true,
      keyboardType: widget.keyboardType,
      decoration: InputDecoration(
        labelText: widget.labelText,
        hintText: widget.hintText,
        icon: Icon(widget.iconName),
        suffixIcon: suffixIconWidget,
        border: OutlineInputBorder(borderSide: BorderSide()),
      ),
    );

    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          enableSuffixIcon = true;
        });
        controller.selection =
            TextSelection(baseOffset: 0, extentOffset: controller.text.length);
      } else {
        setState(() {
          enableSuffixIcon = false;
        });
      }
    });

    //set TextField value if Exists!
    value = controller.text;

    //Save for Global Access
    Input.setValue(widget.id, value);
    return Container(
      padding: EdgeInsets.only(
        bottom: 8.0,
      ),
      child: instance,
    );
  }

  @override
  void dispose() {
    focusNode.dispose();
    controller.dispose();
    super.dispose();
  }
}
