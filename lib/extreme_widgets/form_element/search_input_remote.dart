import 'package:flutter/material.dart';
import 'package:material_search/material_search.dart';
import 'package:premiumtravel/extreme_widgets/helper/input.dart';

class SearchInputRemote extends StatefulWidget {
  final String id;
  final String labelText;
  final IconData iconName;
  final String value;
  final List<String> options;
  final dynamic remoteApi;
  final String displayMember;
  final String valueMember;
  final String infoMember;

  SearchInputRemote({
    Key key,
    this.id,
    this.iconName,
    this.labelText,
    this.value: "",
    this.options,
    this.remoteApi,
    this.displayMember,
    this.valueMember,
    this.infoMember,
  }) : super(key: key);

  @override
  _SearchInputRemoteWidget createState() => new _SearchInputRemoteWidget();
}

class _SearchInputRemoteWidget extends State<SearchInputRemote> {
  List<String> _searchItems = [];

  String _selectedItem = 'No one';
  _buildMaterialSearchPage(BuildContext context) {
    return new MaterialPageRoute<String>(
        settings: new RouteSettings(
          name: 'material_search',
          isInitialRoute: false,
        ),
        builder: (BuildContext context) {
          return new Material(
            child: new MaterialSearch<String>(
              placeholder: 'Search',
              results: _searchItems
                  .map((String v) => new MaterialSearchResult<String>(
                        icon: Icons.flag,
                        value: v,
                        text: v.split("|")[1],
                      ))
                  .toList(),
              filter: (dynamic value, String criteria) {
                return value.toLowerCase().trim().contains(
                    new RegExp(r'' + criteria.toLowerCase().trim() + ''));
              },
              onSelect: (dynamic value) => Navigator.of(context).pop(value),
              onSubmit: (String value) => Navigator.of(context).pop(value),
            ),
          );
        });
  }

  _showMaterialSearch(BuildContext context) {
    Navigator.of(context).push(_buildMaterialSearchPage(context)).then(
      (dynamic value) {
        String rowValue = value.toString().split("|")[0];
        String rowDisplayValue = value.toString().split("|")[1];

        setState(() => _selectedItem = rowDisplayValue);
        Input.setValue(widget.id, rowValue);
      },
    );
  }

  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  FocusNode tempFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    initSearchItems();
  }

  void initSearchItems() {
    if (widget.options != null) {
      _searchItems = widget.options;
      return;
    }

    _searchItems.clear();
    widget.remoteApi().then((value) {
      for (int i = 0; i < value.length; i++) {
        String rowValue = value[i][widget.valueMember];
        String rowDisplayValue = value[i][widget.displayMember];
        String rowInfoValue = value[i][widget.infoMember];

        String rowInfoString = rowInfoValue != null ? " - $rowInfoValue" : "";
        _searchItems.add("$rowValue|$rowDisplayValue$rowInfoString");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    InkWell instance = InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(tempFocusNode);
        _showMaterialSearch(context);
      },
      child: TextField(
        controller: controller,
        focusNode: focusNode,
        enabled: false,
        decoration: InputDecoration(
          labelText: widget.labelText,
          icon: Icon(widget.iconName),
          border: OutlineInputBorder(borderSide: BorderSide()),
        ),
      ),
    );

    controller.text = _selectedItem;

    return Container(
      padding: EdgeInsets.only(
        bottom: 8.0,
      ),
      child: instance,
    );
  }

  @override
  void dispose() {
    focusNode.dispose();
    controller.dispose();
    tempFocusNode.dispose();
    super.dispose();
  }
}
