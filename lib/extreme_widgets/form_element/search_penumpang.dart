import 'dart:async';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:premiumtravel/extreme_widgets/helper/input.dart';

class SearchPenumpang extends StatefulWidget {
  final String id;
  final String labelText;
  final IconData iconName;
  final String value;

  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;

  SearchPenumpang({
    Key key,
    this.id,
    this.iconName,
    this.labelText,
    this.value: "",
    this.initialDate,
    this.firstDate,
    this.lastDate,
  }) : super(key: key);

  @override
  _SearchPenumpangState createState() => new _SearchPenumpangState();
}

class _SearchPenumpangState extends State<SearchPenumpang> {
  int dewasa = 0;
  int anak = 0;
  int bayi = 0;

  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  FocusNode tempFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    InkWell instance = InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(tempFocusNode);
        _showIntDialog();
      },
      child: TextField(
        controller: controller,
        focusNode: focusNode,
        enabled: false,
        decoration: InputDecoration(
          labelText: widget.labelText,
          icon: Icon(widget.iconName),
          border: OutlineInputBorder(borderSide: BorderSide()),
        ),
      ),
    );

    String displayText = "";
    if (dewasa > 0) {
      displayText += "Dewasa: $dewasa, ";
    }

    if (anak > 0) {
      displayText += "Anak: $anak, ";
    }

    if (bayi > 0) {
      displayText += "Bayi: $bayi, ";
    }

    controller.text = displayText;

    return Container(
      padding: EdgeInsets.only(
        bottom: 8.0,
      ),
      child: instance,
    );
  }

  Future _showIntDialog() async {
    await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          color: Colors.transparent,
          child: Container(
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    'Masukkan jumlah penumpang:',
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 14.0,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          'Dewasa',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 12.0,
                            decoration: TextDecoration.none,
                          ),
                        ),
                        NumberPicker.integer(
                          initialValue: dewasa,
                          minValue: 0,
                          maxValue: 4,
                          onChanged: (value) {
                            setState(() {
                              dewasa = value;
                            });
                            Input.setValue("dewasa", value.toString());
                          },
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          'Anak',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 12.0,
                            decoration: TextDecoration.none,
                          ),
                        ),
                        NumberPicker.integer(
                          initialValue: anak,
                          minValue: 0,
                          maxValue: 4,
                          onChanged: (value) {
                            setState(() {
                              anak = value;
                            });
                            Input.setValue("anak", value.toString());
                          },
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          'Bayi',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 12.0,
                            decoration: TextDecoration.none,
                          ),
                        ),
                        NumberPicker.integer(
                          initialValue: bayi,
                          minValue: 0,
                          maxValue: 4,
                          onChanged: (value) {
                            setState(() {
                              bayi = value;
                            });
                            Input.setValue("bayi", value.toString());
                          },
                        ),
                      ],
                    ),
                  ],
                ),
                RaisedButton(
                  child: Text("Close"),
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),
              ],
            ),
          ),
        );
      },
    ).then((value) {});
  }

  @override
  void dispose() {
    focusNode.dispose();
    controller.dispose();
    tempFocusNode.dispose();
    super.dispose();
  }
}
