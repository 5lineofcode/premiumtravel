import 'package:flutter/material.dart';
import 'package:material_search/material_search.dart';
import 'package:premiumtravel/extreme_widgets/helper/input.dart';

class SearchInput extends StatefulWidget {
  final String id;
  final String labelText;
  final IconData iconName;
  final String value;
  final List<String> options;

  SearchInput({
    Key key,
    this.id,
    this.iconName,
    this.labelText,
    this.value: "",
    this.options,
  }) : super(key: key);

  @override
  _SearchInputWidget createState() => new _SearchInputWidget();
}

class _SearchInputWidget extends State<SearchInput> {
  List<String> _searchItems = [];
  String _selectedItem = 'No one';

  _buildMaterialSearchPage(BuildContext context) {
    return new MaterialPageRoute<String>(
        settings: new RouteSettings(
          name: 'material_search',
          isInitialRoute: false,
        ),
        builder: (BuildContext context) {
          return new Material(
            child: new MaterialSearch<String>(
              placeholder: 'Search',
              results: _searchItems
                  .map((String v) => new MaterialSearchResult<String>(
                        icon: Icons.person,
                        value: v,
                        text: "$v",
                      ))
                  .toList(),
              filter: (dynamic value, String criteria) {
                return value.toLowerCase().trim().contains(
                    new RegExp(r'' + criteria.toLowerCase().trim() + ''));
              },
              onSelect: (dynamic value) => Navigator.of(context).pop(value),
              onSubmit: (String value) => Navigator.of(context).pop(value),
            ),
          );
        });
  }

  _showMaterialSearch(BuildContext context) {
    Navigator.of(context)
        .push(_buildMaterialSearchPage(context))
        .then((dynamic value) {
      setState(() => _selectedItem = value as String);
    });
  }

  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  FocusNode tempFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _searchItems = widget.options;
    InkWell instance = InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(tempFocusNode);
        _showMaterialSearch(context);
      },
      child: TextField(
        controller: controller,
        focusNode: focusNode,
        enabled: false,
        decoration: InputDecoration(
          labelText: widget.labelText,
          icon: Icon(widget.iconName),
          border: OutlineInputBorder(borderSide: BorderSide()),
        ),
      ),
    );

    controller.text = _selectedItem;

    //Save for Global Access
    Input.setValue(widget.id, _selectedItem);

    return Container(
      padding: EdgeInsets.only(
        bottom: 8.0,
      ),
      child: instance,
    );
  }

  @override
  void dispose() {
    focusNode.dispose();
    controller.dispose();
    tempFocusNode.dispose();
    super.dispose();
  }
}
