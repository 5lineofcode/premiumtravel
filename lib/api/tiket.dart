import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Tiket {
  static String accessToken = "773676d45d463a5af3d62f9fb9b4b779eee595f3";
  static String baseApiUrl = "https://api-sandbox.tiket.com/";
  static String b = "dea";
  static String businessID = '29018861';
  static String businessName = 'Anagata Studio';
  static String secretKey = '686cefadde5ffe8c45165ad590c861c9';

  static Future<List<dynamic>> getListCountry() async {
    var url =
        "${Tiket.baseApiUrl}general_api/listCountry?output=json&token=$accessToken";
    var response = await http.get(url, headers: {});
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      print("Result Done");
      return result["listCountry"];
    } else {
      return null;
    }
  }

  static Future<List<dynamic>> getAirportList() async {
    var url =
        "${Tiket.baseApiUrl}flight_api/all_airport?token=$accessToken&output=json";
    var response = await http.get(url, headers: {});
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      print("Result Done");
      return result["all_airport"]["airport"];
    } else {
      return null;
    }
  }

  static dynamic depAirport;
  static dynamic arrAirport;
  static dynamic departuresResult;

  static Future<String> searchFlight(
    departureAirportCode,
    arrivalAirportCode,
    departDate,
    returnDate,
    adultCount,
    childCount,
    infantCount,
  ) async {
    var url =
        "${Tiket.baseApiUrl}search/flight?d=$departureAirportCode&a=$arrivalAirportCode&date=$departDate&ret_date=$returnDate&adult=$adultCount&child=$childCount&infant=$infantCount&token=$accessToken&v=1&output=json";
    var response = await http.get(url, headers: {});
    print(url);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      print("SearchFlight Done.. ");

      //depAirport = result["go_det"]["dep_airport"];
      //arrAirport = result["go_det"]["arr_airport"];

      try {
        departuresResult = result["departures"]["result"];
      } catch (e) {
        departuresResult = null;
      }

      return "Done";
    } else {
      return "Failed";
    }
  }

  /*
  searchFlight: async function (req, res) {
        const post = req.body;
        const url = `${this.baseApiUrl}search/flight?d=${post.departureAirportCode}&a=${post.arrivalAirportCode}&date=${post.departDate}&ret_date=${post.returnDate}&adult=${post.adultCount}&child=${post.childCount}&infant=${post.infantCount}&token=${this.accessToken}&v=3&output=json`;
        const response = await axios.get(url);
        const data = response.data;

        res.send(data);
    },
  */
}
