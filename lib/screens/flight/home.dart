import 'package:flutter/material.dart';
import 'package:premiumtravel/api/tiket.dart';
import 'package:premiumtravel/extreme_widgets/form_element/search_date.dart';
import 'package:premiumtravel/extreme_widgets/form_element/search_input_remote.dart';
import 'package:premiumtravel/extreme_widgets/form_element/search_penumpang.dart';
import 'package:premiumtravel/extreme_widgets/helper/context_manager.dart';
import 'package:premiumtravel/extreme_widgets/helper/input.dart';
import 'package:premiumtravel/screens/flight/search_flight.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Widget> formFields = [
      SearchInputRemote(
        id: "kota_asal",
        labelText: "Kota Asal",
        iconName: Icons.flight_takeoff,
        remoteApi: Tiket.getAirportList,
        displayMember: "airport_name",
        valueMember: "airport_code",
        infoMember: "location_name",
      ),
      SearchInputRemote(
        id: "kota_tujuan",
        labelText: "Kota Tujuan",
        iconName: Icons.flight_land,
        remoteApi: Tiket.getAirportList,
        displayMember: "airport_name",
        valueMember: "airport_code",
        infoMember: "location_name",
      ),
      SearchPenumpang(
        labelText: "Jumlah Penumpang",
        iconName: Icons.people_outline,
      ),
      SearchDate(
        id: "tanggal_berangkat",
        labelText: "Tanggal Berangkat",
        iconName: Icons.calendar_today,
      ),
      SearchDate(
        id: "tanggal_pulang",
        labelText: "Tanggal Pulang",
        iconName: Icons.calendar_today,
      ),
      RaisedButton(
        child: Text("Search"),
        onPressed: () {
          ContextManager.context = context;
          Tiket.searchFlight(
            Input.getValue("kota_asal"),
            Input.getValue("kota_tujuan"),
            Input.getValue("tanggal_berangkat"),
            Input.getValue("tanggal_pulang"),
            Input.getValue("dewasa"),
            Input.getValue("anak"),
            Input.getValue("bayi"),
          ).then((value) {
            print("SEARCH RESULT");
            if (Tiket.departuresResult != null) {
              print(Tiket.departuresResult.length);
              print("----");
              print(Tiket.departuresResult[0]);
              print("----");
              print(Tiket.departuresResult[0]["flight_id"]);
              print(Tiket.departuresResult[0]["airlines_name"]);
              print(Tiket.departuresResult[0]["departure_city"]);
              print(Tiket.departuresResult[0]["arrival_city"]);
              print(Tiket.departuresResult[0]["price_value"]);
              print(Tiket.departuresResult[0]["price_adult"]);
              print(Tiket.departuresResult[0]["price_child"]);
              print(Tiket.departuresResult[0]["price_infant"]);
            }
            else{
              print("Result is NULL");
            }
          }).then((result) {
            print("Done..");

            Navigator.push(
              ContextManager.context,
              MaterialPageRoute(builder: (context) => SearchFlightScreen()),
            );
          });
        },
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text("Home Screen"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: Form(
            child: Column(
          children: formFields,
        )),
      ),
    );
  }
}
