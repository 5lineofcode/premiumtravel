import 'package:flutter/material.dart';
import 'package:premiumtravel/api/tiket.dart';

class SearchFlightScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String flightId;
    String airlinesName;
    String priceValue;
    String image;

    return Scaffold(
      appBar: AppBar(
        title: Text("Search Flight Screen"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: ListView.builder(
          itemCount: Tiket.departuresResult.length,
          itemBuilder: (context, index) {
            dynamic selectedValue = Tiket.departuresResult[index];

            flightId = selectedValue["flight_id"];
            airlinesName = selectedValue["airlines_name"];
            priceValue = selectedValue["price_value"];
            image = selectedValue["image"];

            dynamic subtitle = Text('Price: $priceValue');

            dynamic header = ListTile(
              leading: Image.network(image),
              title: Text("$airlinesName"),
              subtitle: subtitle,
            );

            return Card(
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  header,
                  new ButtonTheme.bar(
                    // make buttons use the appropriate styles for cards
                    child: new ButtonBar(
                      children: <Widget>[
                        new FlatButton(
                          child: const Text('BUY TICKETS'),
                          onPressed: () {/* ... */},
                        ),
                        new FlatButton(
                          child: const Text('LISTEN'),
                          onPressed: () {/* ... */},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
